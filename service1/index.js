
const app = require('express')();
const axios = require('axios');

app.get('/ping', async (req,res)=> {
    const response = await axios.get('http://service2:3000/ping') //Here we have specified the internal port 3000 and not the external port 3010
    res.send(response.data);
})

app.listen(3000,'0.0.0.0', () => {
    console.log("Service 1 started...")
})